<?php
    if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
        $currentpage = (int) $_GET['currentpage'];
    } else {
        // страница по умолчанию
        $currentpage = 1;
    } 
    
    //оффсет - с какой записи начать показывать 
    $offset = ($currentpage - 1) * $rowsperpage;    
    
?>