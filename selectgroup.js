$(document).ready(function(){
    $('select').on('focusin', function(){
        console.log("Saving value " + $(this).val());
        $(this).data('val', $(this).val());
    });

    $('select').on('change', function() {
        var id= $(this).attr("id")
        var group=this.value
        var prev = $(this).data('val');
        $.ajax({
            url: '../updategroup.php',
            type: 'POST',
            dataType: 'json',
            data: { student_id: id, group_name: group, previous_val: prev},
            success: function(output){
                console.log(output)
              }
        });
      });

});