$(document).ready(function() {
    //отключить обновление страницы
    $("#attendance_form").submit(function(e) {
        e.preventDefault();
    });

    //собрать и отправить новый пропуск в бд
    $("#submit_btn").click(function(){
        var student = $("#student_picker").find('option:selected').attr("stud_id");
        var stud_fio= $("#student_picker").find('option:selected').val();
        var lesson =$("#title").attr("l_id");
        $.ajax({
            url: '../insertMiss.php',
            type: 'POST',
            dataType: 'json',
            data: {  s_id: student, l_id: lesson},
            success: function(output){
               console.log(output)
               $("#miss_tbody").append('<tr><th scope="row">'+student+'</th><td>'+stud_fio+'</td></tr>');
               $('#msg').fadeIn('slow');
               $('#msg').html("Пропуск успешно добавлен").fadeIn('slow');
               $('#msg').delay(5000).fadeOut('slow');

              }
        });
    });

 });