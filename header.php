<?php

    function getHeader(){
        $path=$_SERVER['PATH_INFO'];
        $g=$s=$t=$d=$l=$sc=$u="";
        switch($path){
            case '/groups': $g="active"; break;
            case '/students': $s="active"; break;
            case '/tutors': $t="active"; break;
            case '/disciplines': $d="active"; break;
            case '/lessons': $l="active"; break;
            case '/schedule': $sc="active"; break;
            case '/users': $u="active"; break;
        }
        echo '
        <header>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Merlin</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link '.$sc.'" href="./schedule">Расписание</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link '.$d.'" href="./disciplines">Дисциплины</a>
                </li>';
                if($_SESSION["role"]=="operator"||$_SESSION["role"]=="admin")
                echo '
                <li class="nav-item">
                    <a class="nav-link '.$g.'" href="./groups">Группы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link '.$s.'" href="./students">Студенты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link '.$t.'" href="./tutors">Преподователи</a>
                </li> 
                <li class="nav-item">
                    <a class="nav-link '.$l.'" href="./lessons">Занятия</a>
                </li>
                '; 
                if($_SESSION["role"]==="admin"){
                    echo '
                    <li class="nav-item">
                        <a class="nav-link '.$u.'" href="./users">Пользователи</a>
                    </li>';
                }
                echo '
            </ul>
            <h4 class="font-weight-normal ml-auto mr-3 p-2" id="username">'.$_SESSION["role"]. ' '  .$_SESSION["username"].'</h4>
            <a class="navbar-brand" href="../logout.php">Выход</a>
            </div>
        </nav>
        </header>';
    }


    
?>