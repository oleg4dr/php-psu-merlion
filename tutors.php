<?php
    function getTutors(){
        require_once 'config.php';
        require_once 'pagination.php';
        require_once 'request.php';
        include 'paginationhelper.php';
    
        $query = mysqli_query($link,"SELECT user_id, userFIO_name, userFIO_surname,userFIO_middle_name 
        FROM users INNER JOIN userfio on users.user_id=userfio.userFIO_id 
        WHERE users.user_role=\"tutor\"  ORDER BY `users`.`user_id` ASC   
        LIMIT $offset, $rowsperpage");
       
        $disciplines_q = mysqli_query($link, "SELECT discipline_id, discipline_name FROM disciplines");
        $diciplines_arr = array();
        while($oneDisc = mysqli_fetch_array($disciplines_q))
            $diciplines_arr[$oneDisc['discipline_id']] = $oneDisc['discipline_name'];

        $tutor_disc_q = mysqli_query($link, "SELECT tutor_discipline_tutor_id, tutor_discipline_discipline_id FROM tutor_discipline");
        $tutor_disc_arr = array();
        while($one_tutor_disc = mysqli_fetch_array($tutor_disc_q)){
            if(!array_key_exists($one_tutor_disc['tutor_discipline_tutor_id'], $tutor_disc_arr))
                $tutor_disc_arr[$one_tutor_disc['tutor_discipline_tutor_id']]=array();           
            array_push($tutor_disc_arr[$one_tutor_disc['tutor_discipline_tutor_id']], $one_tutor_disc['tutor_discipline_discipline_id']); 
        }  

        echo '
        <script src="../dropdowninit.js"></script>       
        <div id="content-table">
        <table class="table table-hover mt-5">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Фамилия</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Отчество</th>
                    <th scope="col">Дисциплины</th>
                </tr>
            </thead>
            <tbody>';
        while ($oneTutor =  mysqli_fetch_array($query)){
            echo '
            <tr>
                <th scope="row">'. $oneTutor['user_id'] .'</th>
                <td>'. $oneTutor['userFIO_surname'] .'</td>
                <td>'. $oneTutor['userFIO_name'] .'</td>
                <td>'. $oneTutor['userFIO_middle_name'] .'</td>
                <td>
                    <select id="'.$oneTutor['user_id'].'" class="selectpicker show-tick" multiple  data-selected-text-format="count" >';
                    foreach($diciplines_arr as $d){
                        $is_selected = ""; 
                        if(in_array(array_search($d, $diciplines_arr), $tutor_disc_arr[$oneTutor['user_id']]))
                            $is_selected="selected";
                        echo '<option '.$is_selected.'>'.$d.'#'.array_search($d, $diciplines_arr).'</option>';
                    }
                        
                echo '
                    </select>
                </td>
            </tr>
            ';
        }
        echo '</tbody>
        </table>
        </div>';

            // addPagination('students', $currentpage, $link, $rowsperpage);
        
        
    }
?>