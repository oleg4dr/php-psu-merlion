$(document).ready(function() {
    $('.selectpicker').selectpicker();
    // ajax сохранение выбранных dropdown пунктов коллом на php файл при изменении статуса select объекта
    $('select').on('change', function() {
        var id= $(this).attr("id")
        var disciplines=$(this).val()
        var d_ids=[]
        disciplines.forEach(element => {
          d_ids.push(element.split('#')[1])
        })
        $.ajax({
            url: '../assigndisciplines.php',
            type: 'POST',
            dataType: 'json',
            data: {  user_id: id, disciplines_list: d_ids},
            success: function(output){
                console.log(output)
              }
        });
      });
 });