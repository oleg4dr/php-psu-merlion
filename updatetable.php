<?php
    require_once 'config.php';
    
    $page = $_REQUEST['page_num'];
    $table = $_REQUEST['table_name'];
    $currentpage = $_REQUEST['current'];
    $offset = ($page - 1) * $rowsperpage; 
    $q = "SELECT * FROM `tutors` ORDER BY `tutors`.`tutor_id` ASC  LIMIT $offset, $rowsperpage";
    $query = mysqli_query($link, $q);
    //собрать и вернуть json
    $result_array_ids=array();
    $result_array_names=array();
    $result_array_surnames=array();
    $result_trs=array();
    $i=0;
    while($res = mysqli_fetch_array($query)) {
        $result_trs[$i++]= '<tr><th>'.$res['tutor_id'].'</th><td>'. $res['tutor_name'] .'</td><td>'. $res['tutor_surname'] .'</td></tr>';
    }

    die(json_encode(array(
        'result_table_rows' => $result_trs
    )));
?>