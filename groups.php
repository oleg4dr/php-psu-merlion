<?php
    function getGroups(){
        require_once 'config.php';
        require_once 'pagination.php';
        include 'paginationhelper.php';
        include 'request.php';
        $query = mysqli_query($link,"SELECT * FROM groups ORDER BY `groups`.`group_id`  ASC  LIMIT $offset, $rowsperpage");
        
        if(isset($_POST['SubmitButton'])){
            // Prepare an update statement
            $sql = "INSERT INTO groups (group_name) VALUES (?)";
            $q = "SELECT * FROM groups WHERE group_name=".'"'.request("group_name").'"';
            $query_check = mysqli_query($link, $q);
            if (mysqli_num_rows($query_check)==1)
                echo "Название группы должно быть уникальным";
            else if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s", $param_groum_name);
                
                // Set parameters
                $param_groum_name = request("group_name");
                
                // Attempt to execute the prepared statement
                if ($param_groum_name)
                {
                    if(mysqli_stmt_execute($stmt))
                        header('Location: '.$_SERVER['REQUEST_URI']);
                }
                else
                {
                    echo "Название группы должно быть от 3 до 15 символов и содержать только кирилиццу.";
                }
            }
        }


        echo '
        <div id="content-table">
        <table class="table table-hover mt-5">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Группа</th>
                </tr>
            </thead>
            <tbody>';
        while ($oneGroup =  mysqli_fetch_array($query)){
            echo '
            <tr>
                <th scope="row">'. $oneGroup['group_id'] .'</th>
                <td>'. $oneGroup['group_name'] .'</td>
            </tr>';
        }
        echo '</tbody>
        </table>
        </div>';
        
        //пагинация
        addPagination('groups', $currentpage, $link, $rowsperpage);

        

        echo '
        <div class="text-center">
            <form class="form" action="" method="post">
                <h4 class="h4 mb-3 font-weight-normal">Добавить группу</h4>
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="group_name" class="form-control">
                    <span class="help-block"></span>
                </div>    
                <div class="form-group">
                    <input type="submit" name="SubmitButton" class="btn btn-primary" value="Отправить" >
                </div>  
            </form>
        </div>
        ';


    }
?>

