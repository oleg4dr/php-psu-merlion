<?php
    function getLessons(){
        require_once 'config.php';
        require_once 'pagination.php';
        include 'paginationhelper.php';

        $groups_q = mysqli_query($link, "SELECT * FROM groups");
        $disc_q = mysqli_query($link, "SELECT discipline_id, discipline_name FROM disciplines");
        $aud_q = mysqli_query($link, "SELECT * from auditories");
        echo '
            <script src="../getValidTutors.js"></script>   
            <form class="mt-5" action="" method="post" id="lesson_form">
                <h1 class="h3 text-center  font-weight-normal">Добавить занятие</h1>
                <select class="custom-select" id="group_sel">
                    <option  value="" disabled selected>Группа</option>';
                    while($oneGroup = mysqli_fetch_array($groups_q))
                        echo '<option group_id="'.$oneGroup['group_id'].'">'.$oneGroup['group_name'].'</option>';
                echo '
                </select>   
                <div class="form-row mt-3">
                    <div class="col">
                        <select class="custom-select" id="discipline_selector">
                            <option value="" disabled selected>Дисциплина</option>';
                            while($oneDisc = mysqli_fetch_array($disc_q))
                        echo '<option disc_id="'.$oneDisc['discipline_id'].'">'.$oneDisc['discipline_name'].'#'.$oneDisc['discipline_id'].'</option>';
                echo '  </select>
                    </div>
                    <div class="col">
                        <select class="custom-select" id="tutor_selector">
                            <option value="" disabled selected>Преподователь</option>                           
                        </select>
                    </div>
                </div>
                <div class="form-row mt-3">
                    <div class="col">
                        <div class="input-group date" data-provide="datepicker" id="my_datepicker">
                            <input type="text" class="form-control" placeholder="Дата" id="date_val">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th" id="datepicker_f"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <select class="custom-select" id="my_timepicker">
                            <option value="" disabled selected>Время</option>
                            <option>8:00</option>
                            <option>9:45</option>
                            <option>10:30</option>
                            <option>11:20</option>
                            <option>13:00</option>
                            <option>14:50</option>
                        </select>
                    </div>
                    <div class="col">
                        <select class="custom-select" id="aud_val">
                            <option value="" disabled selected>Аудитория</option>';
                            while($oneAud = mysqli_fetch_array($aud_q))
                                echo '<option aud_id="'.$oneAud['auditory_id'].'">'.$oneAud['auditory_num'].'</option>';
                        echo '
                        </select>
                    </div>
                </div>
                <div class="form-group mt-3 text-center">
                    <input type="submit" class="btn btn-primary" id="submit_btn" value="Добавить">
                    <input type="reset" class="btn btn-default" value="Сброс">
                </div>
                <div class="container">
                    <div id="msg" class="bg-success text-white text-center rounded row justify-content-center"></div>
                </div> 
            </form>
                 
        ';
    }
?>