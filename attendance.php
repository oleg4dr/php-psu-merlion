<?php
    function getAttendance(){
        require_once 'config.php';
        require_once 'request.php';
        $lesson_id = (int) $_GET['lesson_id'];
        $role = $_SESSION["role"];
        $query = mysqli_query($link,"SELECT userfio.userFIO_id, userfio.userFIO_surname, userfio.userFIO_name, userfio.userFIO_middle_name, students.group_id
        FROM userfio INNER JOIN students ON userfio.userFIO_id=students.student_id
        WHERE students.group_id=(SELECT lessons.lesson_group_id FROM lessons WHERE lessons.lesson_id=$lesson_id)");
        $miss_q=mysqli_query($link, "SELECT * FROM attendance INNER JOIN userfio on attendance_student_id=userfio.userFIO_id WHERE attendance_lesson_id=$lesson_id");

        echo '
        <h4 id="title" class="text text-center mt-3" l_id="'.$lesson_id.'">Пропуски для занятия#'.$lesson_id.'</h4>
        <div id="content-table">
        <table class="table table-hover mt-5">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Студент</th>
                </tr>
            </thead>
            <tbody id="miss_tbody">';
        while ($oneS =  mysqli_fetch_array($miss_q)){      
                echo '
                    <tr>
                        <th scope="row">'. $oneS['attendance_student_id'] .'</th>
                        <td>'. $oneS['userFIO_surname']." ".$oneS['userFIO_name']." ".$oneS['userFIO_middle_name'].'</td>
                    </tr>';
        }
        echo '
        </tbody>
        </table>
        </div>';
        if($role!="student")
        echo '
        <script src="../setAttendance.js"></script>   
        <form class="mt-5" action="" method="post" id="attendance_form">
            <h1 class="h3 text-center  font-weight-normal">Добавить пропуск</h1>
            <div class="form-row mt-3">
                <div class="col">
                <select class="custom-select" id="student_picker">
                    <option value="" disabled selected>Студент</option>';
                    while($oneStudent = mysqli_fetch_array($query)){
                        echo '<option stud_id="'.$oneStudent['userFIO_id'].'">'.$oneStudent['userFIO_surname']." ".$oneStudent['userFIO_name']." ".$oneStudent['userFIO_middle_name'].'</option>';
                    }
                echo '
                </select>
                </div>
            </div>
            <div class="form-group mt-3 text-center">
                <input type="submit" class="btn btn-primary" id="submit_btn" value="Добавить">
            </div>
            <div class="container">
                <div id="msg" class="bg-success text-white text-center rounded row justify-content-center"></div>
            </div> 
        </form>
    ';
    }
?>