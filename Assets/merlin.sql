-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2020 at 02:51 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `merlin`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `attendance_id` int(255) NOT NULL,
  `attendance_lesson_id` int(255) NOT NULL,
  `attendance_student_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`attendance_id`, `attendance_lesson_id`, `attendance_student_id`) VALUES
(1, 6, 6),
(4, 12, 7),
(5, 9, 7),
(6, 10, 6),
(7, 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `auditories`
--

CREATE TABLE `auditories` (
  `auditory_id` int(255) NOT NULL,
  `auditory_num` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auditories`
--

INSERT INTO `auditories` (`auditory_id`, `auditory_num`) VALUES
(1, 25),
(2, 14),
(3, 33),
(4, 56),
(5, 2),
(6, 414);

-- --------------------------------------------------------

--
-- Table structure for table `disciplines`
--

CREATE TABLE `disciplines` (
  `discipline_id` int(255) NOT NULL,
  `discipline_name` varchar(535) NOT NULL,
  `discipline_umk` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `disciplines`
--

INSERT INTO `disciplines` (`discipline_id`, `discipline_name`, `discipline_umk`) VALUES
(3, 'Химия', 'bolt.docx'),
(4, 'Математика', 'Math-2020.docx'),
(5, 'Спортивная ориентация на местности', 'Chemisty-2019.docx');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `grade_id` int(255) NOT NULL,
  `grade_disc_id` int(255) NOT NULL,
  `grade_student_id` int(255) NOT NULL,
  `grade_kt` int(255) NOT NULL,
  `grade_val` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`grade_id`, `grade_disc_id`, `grade_student_id`, `grade_kt`, `grade_val`) VALUES
(2, 3, 6, 1, '4'),
(3, 3, 7, 5, '3'),
(4, 3, 6, 4, '5'),
(5, 3, 7, 2, '4'),
(6, 3, 7, 3, '4'),
(7, 3, 6, 4, '5'),
(8, 3, 6, 4, '5');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(255) NOT NULL,
  `group_name` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`) VALUES
(30, 'ММТ-2017'),
(31, 'ТПЗ-2019');

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE `lessons` (
  `lesson_id` int(255) NOT NULL,
  `lesson_group_id` int(255) NOT NULL,
  `lesson_discipline_id` int(255) NOT NULL,
  `lesson_tutor_id` int(255) NOT NULL,
  `lesson_date` date NOT NULL,
  `lesson_time` varchar(535) NOT NULL,
  `lesson_auditory_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lessons`
--

INSERT INTO `lessons` (`lesson_id`, `lesson_group_id`, `lesson_discipline_id`, `lesson_tutor_id`, `lesson_date`, `lesson_time`, `lesson_auditory_id`) VALUES
(6, 30, 3, 3, '2020-06-24', '9:45', 3),
(7, 30, 3, 3, '2020-06-24', '9:45', 6),
(8, 30, 3, 3, '2020-06-24', '14:50', 6),
(9, 31, 3, 3, '2020-06-26', '10:30', 3),
(10, 31, 3, 3, '2020-06-22', '9:45', 2),
(11, 30, 3, 3, '2020-06-19', '9:45', 2),
(12, 31, 3, 5, '2020-06-19', '10:30', 2),
(13, 31, 3, 5, '2020-06-19', '9:45', 2),
(14, 30, 3, 3, '2020-06-12', '9:45', 3);

-- --------------------------------------------------------

--
-- Table structure for table `page_body_function`
--

CREATE TABLE `page_body_function` (
  `id` int(255) NOT NULL,
  `page_path` varchar(535) NOT NULL,
  `body_function_name` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `page_body_function`
--

INSERT INTO `page_body_function` (`id`, `page_path`, `body_function_name`) VALUES
(1, '/schedule', 'getSchedule'),
(2, '/students', 'getStudentList'),
(3, '/groups', 'getGroups'),
(4, '/tutors', 'getTutors'),
(5, '/users', 'getUsers'),
(7, '/disciplines', 'getDisciplines'),
(8, '/user_details', 'getUserDetails'),
(9, '/lessons', 'getLessons'),
(10, '/attendance', 'getAttendance'),
(11, '/grades', 'getGrades');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(255) NOT NULL,
  `group_id` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `group_id`) VALUES
(6, 30),
(7, 31);

-- --------------------------------------------------------

--
-- Table structure for table `timeslots`
--

CREATE TABLE `timeslots` (
  `timeslot_id` int(255) NOT NULL,
  `timeslot_start_time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tutors`
--

CREATE TABLE `tutors` (
  `tutor_id` int(255) NOT NULL,
  `tutor_status` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tutor_discipline`
--

CREATE TABLE `tutor_discipline` (
  `tutor_discipline_id` int(255) NOT NULL,
  `tutor_discipline_tutor_id` int(255) NOT NULL,
  `tutor_discipline_discipline_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tutor_discipline`
--

INSERT INTO `tutor_discipline` (`tutor_discipline_id`, `tutor_discipline_tutor_id`, `tutor_discipline_discipline_id`) VALUES
(53, 3, 3),
(50, 3, 4),
(45, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `userfio`
--

CREATE TABLE `userfio` (
  `userFIO_id` int(255) NOT NULL,
  `userFIO_name` varchar(535) NOT NULL,
  `userFIO_surname` varchar(535) NOT NULL,
  `userFIO_middle_name` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `userfio`
--

INSERT INTO `userfio` (`userFIO_id`, `userFIO_name`, `userFIO_surname`, `userFIO_middle_name`) VALUES
(3, 'Анатолий', 'Леонтьев', 'Павлович'),
(5, 'Павел', 'Петров', 'Олегович'),
(6, 'Олег', 'Дрочнев', 'Юрьевич'),
(7, 'Вадим', 'Вольцех', 'Вячеславович');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(255) NOT NULL,
  `user_login` varchar(535) NOT NULL,
  `user_password` varchar(535) NOT NULL,
  `user_role` varchar(535) NOT NULL DEFAULT 'student'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_password`, `user_role`) VALUES
(3, 'OlegUser', '$2y$10$R17thEDd3sYgVr3IKAt5BO74T8cYmPpjdj7XwhsAVrKUPA0q3n8H2', 'tutor'),
(4, 'OlegAdmin', '$2y$10$3QzTHljsS0JNOj1HX5L1W.x3sufVF7oJmCuAHfPTazczZWTVH29OS', 'admin'),
(5, 'OlegUser2', '$2y$10$zblbATdL.57typU8Nctdb.4Eknlao6ZHXKI8WgJrrYFcBvsLguIAy', 'tutor'),
(6, 'Oleg1', '$2y$10$sxlkcQZm.vcZAMyDSGGsQumflg.In/kbj49ufEO7V9dl6soVxxsZe', 'student'),
(7, 'Wolceh', '$2y$10$VED5VhqG1ge8j3wYrWcscuisAKWqRdK1gomEvbkFe2epylRHYU332', 'student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `auditories`
--
ALTER TABLE `auditories`
  ADD PRIMARY KEY (`auditory_id`),
  ADD KEY `auditory_id` (`auditory_id`);

--
-- Indexes for table `disciplines`
--
ALTER TABLE `disciplines`
  ADD PRIMARY KEY (`discipline_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`grade_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `group_name` (`group_name`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`lesson_id`),
  ADD KEY `lesson_id` (`lesson_id`),
  ADD KEY `lesson_group_id` (`lesson_group_id`,`lesson_discipline_id`,`lesson_tutor_id`,`lesson_time`,`lesson_auditory_id`),
  ADD KEY `lesson_auditory_id` (`lesson_auditory_id`),
  ADD KEY `lesson_tutor_id` (`lesson_tutor_id`),
  ADD KEY `lesson_timeslot_id` (`lesson_time`),
  ADD KEY `lesson_discipline_id` (`lesson_discipline_id`);

--
-- Indexes for table `page_body_function`
--
ALTER TABLE `page_body_function`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `timeslots`
--
ALTER TABLE `timeslots`
  ADD PRIMARY KEY (`timeslot_id`),
  ADD KEY `timeslot_id` (`timeslot_id`);

--
-- Indexes for table `tutors`
--
ALTER TABLE `tutors`
  ADD PRIMARY KEY (`tutor_id`),
  ADD KEY `tutor_id` (`tutor_id`);

--
-- Indexes for table `tutor_discipline`
--
ALTER TABLE `tutor_discipline`
  ADD PRIMARY KEY (`tutor_discipline_id`),
  ADD UNIQUE KEY `tutor_discipline_tutor_id` (`tutor_discipline_tutor_id`,`tutor_discipline_discipline_id`),
  ADD KEY `tutor_discipline_discipline_id` (`tutor_discipline_discipline_id`);

--
-- Indexes for table `userfio`
--
ALTER TABLE `userfio`
  ADD PRIMARY KEY (`userFIO_id`),
  ADD KEY `userFIO_id` (`userFIO_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `attendance_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `auditories`
--
ALTER TABLE `auditories`
  MODIFY `auditory_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `disciplines`
--
ALTER TABLE `disciplines`
  MODIFY `discipline_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `grade_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `lessons`
--
ALTER TABLE `lessons`
  MODIFY `lesson_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `page_body_function`
--
ALTER TABLE `page_body_function`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `timeslots`
--
ALTER TABLE `timeslots`
  MODIFY `timeslot_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tutor_discipline`
--
ALTER TABLE `tutor_discipline`
  MODIFY `tutor_discipline_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `lessons_ibfk_1` FOREIGN KEY (`lesson_group_id`) REFERENCES `groups` (`group_id`),
  ADD CONSTRAINT `lessons_ibfk_2` FOREIGN KEY (`lesson_auditory_id`) REFERENCES `auditories` (`auditory_id`),
  ADD CONSTRAINT `lessons_ibfk_5` FOREIGN KEY (`lesson_discipline_id`) REFERENCES `disciplines` (`discipline_id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `students_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`);

--
-- Constraints for table `tutor_discipline`
--
ALTER TABLE `tutor_discipline`
  ADD CONSTRAINT `tutor_discipline_ibfk_1` FOREIGN KEY (`tutor_discipline_discipline_id`) REFERENCES `disciplines` (`discipline_id`),
  ADD CONSTRAINT `tutor_discipline_ibfk_2` FOREIGN KEY (`tutor_discipline_tutor_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `userfio`
--
ALTER TABLE `userfio`
  ADD CONSTRAINT `userfio_ibfk_1` FOREIGN KEY (`userFIO_id`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
