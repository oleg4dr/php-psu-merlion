<?php
    session_start();
    require_once('head.php');
    require_once('header.php');
    require_once('footer.php');
    require_once('groups.php');
    require_once('students.php');
    require_once('tutors.php');
    require_once('users.php');
    require_once 'lessons.php';
    require_once 'userdetails.php';
    require_once 'disciplines.php';
    require_once 'schedule.php';
    require_once 'attendance.php';
    require_once 'grades.php';
    $connection = mysqli_connect('127.0.0.1', 'root', '', 'merlin');
        if ($connection == false)
    {
        echo 'Не удалось подключиться!';
        exit();
    }

    
    function getFunctionName(){
        
        $path=$_SERVER['PATH_INFO'];
        $query = mysqli_query($GLOBALS['connection'],"SELECT id, body_function_name FROM page_body_function WHERE page_path='$path'");
        $data = mysqli_fetch_assoc($query);

        return $data['body_function_name'];
    }


    function getMenu(){
        print '<p>Функция меню</p>';
    }


    function generateHtml(){
        echo '<html>';
        getHead();
        echo '<body>';
        getHeader();
        echo '<main role="main" class="container">';
         $body = getFunctionName(); 
         $body();
         echo '</main>';
        getFooter();
        echo '
        </body>
        </html>';
    }

    generateHtml();
?>

