<?php
    require_once 'config.php';
    
    $group = $_POST['group_id'];
    $q="SELECT students.student_id, userfio.userFIO_surname, userfio.userFIO_name, userfio.userFIO_middle_name from groups INNER JOIN students on groups.group_id=students.group_id INNER JOIN userfio on userfio.userFIO_id=students.student_id where groups.group_id=$group";
    $query = mysqli_query($link, $q);
    $res=array();
    while($oneStud=mysqli_fetch_array($query))
        $res[$oneStud['student_id']] = '<option stud_id="'.$oneStud['student_id'].'">'. $oneStud['userFIO_surname'] ." ". $oneStud['userFIO_name']." ". $oneStud['userFIO_middle_name'] . '</option>';
    die(json_encode(array(
        'result' => $res
    )));

?>