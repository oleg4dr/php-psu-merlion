<?php
    function getStudentList(){
        require_once 'config.php';
        require_once 'pagination.php';
        include 'paginationhelper.php';
              
        $query = mysqli_query($link,"SELECT user_id, userFIO_name, userFIO_surname,userFIO_middle_name, IFNULL(students.group_id, \"Не назначена\") AS \"group_id\", group_name
        from users INNER JOIN userfio on users.user_id=userfio.userFIO_id 
        LEFT JOIN students on users.user_id=students.student_id
        LEFT JOIN groups ON students.group_id=groups.group_id
        WHERE users.user_role='student' ORDER BY `users`.`user_id` ASC   
        LIMIT $offset, $rowsperpage");
       
        $query_all_groups= mysqli_query($link, "SELECT group_name FROM groups");
        $all_groups = array();
        while($oneGroup=mysqli_fetch_array($query_all_groups))
            $all_groups[]=$oneGroup['group_name'];
        echo '
        <script src="../selectgroup.js"></script>
        <div id="content-table">
        <table class="table table-hover mt-5">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Фамилия</th>
                    <th scope="col">Имя</th>
                    <th scope="col">Отчество</th>
                    <th scope="col">Группа</th>
                </tr>
            </thead>
            <tbody>';
        while ($oneStudent =  mysqli_fetch_array($query)){
            $group="Не назначена";
            if ($oneStudent['group_name'])
                $group= $oneStudent['group_name'];
            echo '
            <tr>
                <th scope="row">'. $oneStudent['user_id'] .'</th>
                <td>'. $oneStudent['userFIO_surname'] .'</td>
                <td>'. $oneStudent['userFIO_name'] .'</td>
                <td>'. $oneStudent['userFIO_middle_name'] .'</td>
                <td>
                    <select id="'.$oneStudent['user_id'].'" class="custom-select ">
                        <option selected>'.$group.'</option>';
                        foreach ($all_groups as $val)
                            if($group!=$val)
                                echo '<option>'.$val.'</option>';
                echo '</select>
                </td>
            </tr>
            ';
        }
        echo '</tbody>
        </table>
        </div>';

            addPagination('students', $currentpage, $link, $rowsperpage);
    }
?>