<?php
    function getSchedule(){
        $role = $_SESSION["role"];
        $u_id=$_SESSION['id'];
        require_once 'config.php';
        require_once 'pagination.php';
        include 'paginationhelper.php';
        $query="";
        if($role=="admin" || $role=="operator")
        {
            $query = mysqli_query($link,"SELECT lesson_id, groups.group_name, disciplines.discipline_name, userfio.userFIO_surname, userfio.userFIO_name, userfio.userFIO_middle_name, lesson_date, lesson_time, auditories.auditory_num FROM lessons
            INNER JOIN groups ON lessons.lesson_group_id=groups.group_id
            INNER JOIN disciplines on disciplines.discipline_id=lessons.lesson_discipline_id
            INNER JOIN userfio on lessons.lesson_tutor_id=userfio.userFIO_id
            INNER JOIN auditories on lessons.lesson_auditory_id=auditories.auditory_id ORDER BY `lessons`.`lesson_id` ASC LIMIT $offset, $rowsperpage");
        }
        else if ($role=="tutor")
            $query=mysqli_query($link, "SELECT lesson_id, groups.group_name, disciplines.discipline_name, userfio.userFIO_surname, userfio.userFIO_name, userfio.userFIO_middle_name, lesson_date, lesson_time, auditories.auditory_num FROM lessons
            INNER JOIN groups ON lessons.lesson_group_id=groups.group_id
            INNER JOIN disciplines on disciplines.discipline_id=lessons.lesson_discipline_id
            INNER JOIN userfio on lessons.lesson_tutor_id=userfio.userFIO_id
            INNER JOIN auditories on lessons.lesson_auditory_id=auditories.auditory_id
            WHERE lessons.lesson_tutor_id=$u_id
            ORDER BY `lessons`.`lesson_id` ASC LIMIT $offset, $rowsperpage
            ");
        else if ($role=="student")
        {
            $query=mysqli_query($link, "SELECT lesson_id, groups.group_name, disciplines.discipline_name, userfio.userFIO_surname, userfio.userFIO_name, userfio.userFIO_middle_name, lesson_date, lesson_time, auditories.auditory_num FROM lessons
            INNER JOIN groups ON lessons.lesson_group_id=groups.group_id
            INNER JOIN disciplines on disciplines.discipline_id=lessons.lesson_discipline_id
            INNER JOIN userfio on lessons.lesson_tutor_id=userfio.userFIO_id
            INNER JOIN auditories on lessons.lesson_auditory_id=auditories.auditory_id
            WHERE (select students.group_id from students WHERE students.student_id=$u_id)=lessons.lesson_group_id
            ORDER BY `lessons`.`lesson_id`
            ");
        }
            
        echo '
        <div id="content-table">
        <table class="table table-hover mt-5">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Группа</th>
                    <th scope="col">Дисциплина</th>
                    <th scope="col">Преподователь</th>
                    <th scope="col">Дата</th>
                    <th scope="col">Время</th>
                    <th scope="col">Аудитория</th>';
                    if($role!="student")
                        echo 
                        '<th scope="col">Пропуски</th>';
                    echo'
                </tr>
            </thead>
            <tbody>';
            while ($oneLesson =  mysqli_fetch_array($query)){
                $N=substr($oneLesson['userFIO_name'], 0, 2);
                $M=substr($oneLesson['userFIO_middle_name'], 0, 2);
                echo '
                <tr>
                    <th scope="row">'. $oneLesson['lesson_id'] .'</th>
                    <td>'. $oneLesson['group_name'] .'</td>
                    <td>'. $oneLesson['discipline_name'] .'</td>
                    <td>'. $oneLesson['userFIO_surname']." ".$N.".".$M.".".'</td>
                    <td>'. $oneLesson['lesson_date'] .'</td>
                    <td>'. $oneLesson['lesson_time'] .'</td>
                    <td>'. $oneLesson['auditory_num'] .'</td>';
                    if($role!="student")
                        echo'
                        <td> 
                            <a class="btn btn-primary" href="./attendance?lesson_id='.$oneLesson['lesson_id'].'" role="button">Открыть</a>
                        </td>';
                echo'
                </tr>';
            }
            echo '</tbody>
            </table>
            </div>';
    }
?>