$(document).ready(function() {
    //отключить обновление страницы
    $("#grade_form").submit(function(e) {
        e.preventDefault();
    });

    //собрать и отправить новое занятие в бд
    $("#submit_btn").click(function(){
        var disc =$("#title").attr("dis_id");
        var group_name = $("#my_grouppicker").find('option:selected').val();
        var stud = $("#my_studentpicker").find('option:selected').attr("stud_id");
        var stud_fio = $("#my_studentpicker").find('option:selected').val();
        var kt = $("#my_ktpicker").find('option:selected').val();
        var grade = $("#my_gradepicker").find('option:selected').val();
        $.ajax({
            url: '../insertGrade.php',
            type: 'POST',
            dataType: 'json',
            data: {  disc_id: disc, stud_id: stud, kt_val: kt, grade_val: grade },
            success: function(output){
               console.log(output)
               $("#grade_tbody").append('<tr><th scope="row">'+output.result+'</th><td>'+group_name+'</td><td>'+stud_fio+'</td><td>'+kt+'</td><td>'+grade+'</td></tr>');
              }
        });
    });

    $('#my_grouppicker').on('change', function() {
        var group=$("#my_grouppicker").find('option:selected').attr("g_id");
        $('#my_studentpicker :first-child').nextAll().remove();
        $.ajax({
            url: '../getStudents.php',
            type: 'POST',
            dataType: 'json',
            data: {  group_id: group},
            success: function(output){
                console.log(output);
                var newS = Object.values(output.result);
                newS.forEach(element => {
                    $("#my_studentpicker").append(element)
                });
              }
        });
      });
 });