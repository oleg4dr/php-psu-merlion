$(document).ready(function() {
    $('#my_datepicker').datepicker({
        language: "ru",
        orientation: "bottom auto",
        daysOfWeekDisabled: "0",
        calendarWeeks: true,
        autoclose: true,      
        todayHighlight: true,
        toggleActive: true
    });

    //отключить обновление страницы
    $("#lesson_form").submit(function(e) {
        e.preventDefault();
    });

    //собрать и отправить новое занятие в бд
    $("#submit_btn").click(function(){
        var group = $("#group_sel").find('option:selected').attr("group_id");
        var disc = $("#discipline_selector").find('option:selected').attr("disc_id");
        var tut = $("#tutor_selector").find('option:selected').attr("tut_id");
        var date = $("#date_val").val();
        var time = $("#my_timepicker").val();
        var aud = $("#aud_val").find('option:selected').attr("aud_id");
        $.ajax({
            url: '../insertLesson.php',
            type: 'POST',
            dataType: 'json',
            data: {  group_id: group, discipline_id: disc, tut_id: tut, date_val: date, time_val: time, aud_id: aud },
            success: function(output){
               console.log(output)
               $('#msg').fadeIn('slow');
               $('#msg').html("Занятие успешно добавлено").fadeIn('slow');
               $('#msg').delay(5000).fadeOut('slow');
              }
        });
    });

    $('#discipline_selector').on('change', function() {
        var discipline=$(this).val().split('#')[1];
        $('#tutor_selector :first-child').nextAll().remove();
        //очистить текущих преподов
        $.ajax({
            url: '../getTutors.php',
            type: 'POST',
            dataType: 'json',
            data: {  discipline_id: discipline},
            success: function(output){
                var newT = Object.values(output.result);
                newT.forEach(element => {
                    $("#tutor_selector").append(element)
                });
              }
        });
      });
 });