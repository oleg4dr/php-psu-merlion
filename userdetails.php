<?php
    function getUserDetails(){
        require_once 'config.php';
        require_once 'request.php';
        $user_id = (int) $_GET['user_id'];
        $q = "SELECT * FROM `users` WHERE user_id=$user_id";
        $query = mysqli_query($link, $q);
        $res = mysqli_fetch_array($query);
        echo '
        <div class="">
            <h3 class="text mt-5">ФИО пользователя: '.$res['user_login'].'</h3>
        </div>';
        $q = "SELECT * FROM userfio WHERE userfio.userFIO_id=$user_id ";
        $query = mysqli_query($link, $q);
        $res = mysqli_fetch_array($query);
        $name="";
        $surname="";
        $middle_name="";
        $exitst=false;
        //если уже есть данные то подставить их в поля ФИО
        if (mysqli_num_rows($query)==1){ 
            $name=$res['userFIO_name'];
            $surname=$res['userFIO_surname'];
            $middle_name=$res['userFIO_middle_name'];
            $exitst=true;
        }

        if(isset($_POST['SubmitButton'])){
            // если существует то обновить иначе вставить
            if($exitst)
                $sql="UPDATE userfio SET userFIO_name = ?, userFIO_surname=?, userFIO_middle_name=? WHERE userFIO_id=$user_id";
            else
                $sql = "INSERT INTO userfio (userFIO_id, userFIO_name, userFIO_surname, userFIO_middle_name) VALUES ($user_id ,?, ?, ?)";
             if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "sss", $param_user_name, $param_user_surname, $param_user_middle_name);
                
                // Set parameters
                $param_user_name = request("user_name", 'name');
                $param_user_surname = request("user_surname", 'name');
                $param_user_middle_name = request("user_middle_name", 'name');

                // Attempt to execute the prepared statement
                if ($param_user_name && $param_user_surname && $param_user_middle_name)
                {
                    if(mysqli_stmt_execute($stmt))
                        echo "ФИО изменено";
                    $name=$param_user_name;
                    $surname=$param_user_surname;
                    $middle_name=$param_user_middle_name;
                }
                else
                {
                    echo "Имя/фамилия/Отчество должны быть от 3 до 15 символов и содержать только кирилиццу.";
                }
            }
            else 
                echo $sql;
        }

        echo '
        <div class="text-center">
            <form class="form" action="" method="post">
                <h4 class="h4 mb-3 font-weight-normal">Изменить ФИО</h4>
                <div class="form-group">
                    <label>Имя</label>
                    <input type="text" name="user_name" class="form-control" value="'.$name.'">
                    <span class="help-block"></span>
                </div>    
                <div class="form-group">
                    <label>Фамилия</label>
                    <input type="text" name="user_surname" class="form-control" value="'.$surname.'">
                    <span class="help-block"></span>
                </div>
                <div class="form-group">
                    <label>Отчество</label>
                    <input type="text" name="user_middle_name" class="form-control" value="'.$middle_name.'">
                    <span class="help-block"></span>
                </div>
                <div class="form-group">
                    <input type="submit" name="SubmitButton" class="btn btn-primary" value="Отправить" >
                </div>  
            </form>
        </div>
        ';
    }
?>