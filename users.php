<?php
    function getUsers(){
        
        //если ПОЛЬЗОВАТЕЛЬ прошел по урл адресу просто ничего не возвращать 
        if($_SESSION["role"]!=="admin"){
            return;
        }

        require_once 'config.php';
        require_once 'pagination.php';
        include 'paginationhelper.php';  
    
        $query = mysqli_query($link,"SELECT * FROM `users` ORDER BY `users`.`user_id` ASC  LIMIT $offset, $rowsperpage");
        $all_roles = array('student', 'tutor', 'operator', 'admin');
        echo '
        <script src="../editRole.js"></script>
        <div id="content-table">
        <table class="table table-hover mt-5">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Логин</th>
                    <th scope="col">Роль</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>';
        while ($oneUser =  mysqli_fetch_array($query)){
            if ($oneUser['user_id']!=$_SESSION['id']){
                echo '
                    <tr>
                        <th scope="row">'. $oneUser['user_id'] .'</th>
                        <td>'. $oneUser['user_login'] .'</td>
                        <td>
                            <select id="'.$oneUser['user_id'].'" class="custom-select ">
                                <option selected>'.$oneUser['user_role'].'</option>';
                                foreach ($all_roles as $val)
                                    if($val!=$oneUser['user_role'])
                                        echo '<option>'.$val.'</option>';
                            echo '
                            </select>
                        </td>
                        <td>
                            <a class="btn btn-primary" href="./user_details?user_id='.$oneUser['user_id'].'" role="button">ФИО</a>
                        </td>
                    </tr>';
            }
        }
        echo '
        </tbody>
        </table>
        </div>';

        
        //пагинация
        addPagination('users', $currentpage, $link, $rowsperpage);
    }
?>