$(document).ready(function(){
    $(".page-item").click(function(){
        var page_link = $(this).children().attr("targetPage")
        var next_active_page = $(this).addClass("active")
        var page = page_link.substr(page_link.length-1)
        var current_page = $(".active").children().attr("targetPage")
        $.ajax({
            url: '../updatetable.php',
            type: 'POST',
            dataType: 'json',
            data: { page_num: page, table_name: 'tutors', current: current_page },
            success: function(output){
                $("li").removeClass("active");
                $(next_active_page).addClass("active");
                $("#tutor-table-body tr").remove();
                var rows = output.result_table_rows;
                console.log(rows);
                rows.forEach(element => {
                    $("#tutor-table-body").append(element);
                });
                

              }
        });
    });
    
});