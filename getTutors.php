<?php

    require_once 'config.php';
    
    $discipline = $_POST['discipline_id'];
    $q="SELECT users.user_id, userfio.userFIO_surname, userfio.userFIO_name, userfio.userFIO_middle_name, tutor_discipline.tutor_discipline_discipline_id FROM tutor_discipline 
    INNER JOIN users ON tutor_discipline.tutor_discipline_tutor_id=users.user_id
    LEFT JOIN userfio on users.user_id=userfio.userFIO_id
    WHERE tutor_discipline.tutor_discipline_discipline_id=$discipline AND users.user_role='tutor'";
    $query = mysqli_query($link, $q);
    $res=array();
    while($oneTutor=mysqli_fetch_array($query))
        $res[$oneTutor['user_id']] = '<option tut_id="'.$oneTutor['user_id'].'">'. $oneTutor['userFIO_surname'] ." ". $oneTutor['userFIO_name']." ". $oneTutor['userFIO_middle_name'] . '</option>';
    die(json_encode(array(
        'result' => $res
    )));

?>