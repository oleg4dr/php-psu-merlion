<?php
    function getDisciplines(){
        require_once 'config.php';
        require_once 'request.php';
        require_once 'pagination.php';
        include 'paginationhelper.php';
        $role = $_SESSION["role"];
        $query = mysqli_query($link,"SELECT * FROM `disciplines` ORDER BY `disciplines`.`discipline_id` ASC  LIMIT $offset, $rowsperpage");
        echo '
        <div id="content-table">
        <table class="table mt-5 table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Дисциплина</th>
                    <th scope="col">УМК</th>
                    <th scope="col">Успеваемость</th>
                </tr>
            </thead>
            <tbody>';
        while ($oneDiscipline =  mysqli_fetch_array($query)){
            $umk = urlencode($oneDiscipline['discipline_umk']);
            echo '
            <tr>
                <th scope="row">'. $oneDiscipline['discipline_id'] .'</th>
                <td>'. $oneDiscipline['discipline_name'] .'</td>
                <td><a href="../download.php?file=' . $umk . '">'.$oneDiscipline['discipline_umk'].'</a></td>
                <td> 
                    <a class="btn btn-primary" href="./grades?disc_id='.$oneDiscipline['discipline_id'].'" role="button">Открыть</a>
                </td>
            </tr>
            ';
        }
        echo '
        </tbody>
        </table>
        </div>';

        addPagination('disciplines', $currentpage, $link, $rowsperpage);

        if($_SERVER["REQUEST_METHOD"] == "POST"){
            // Check if file was uploaded without errors
            if(isset($_FILES["UMK"]) && $_FILES["UMK"]["error"] == 0){
                $allowed = array("doc" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",  "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                $filename = $_FILES["UMK"]["name"];
                $filetype = $_FILES["UMK"]["type"];
                $filesize = $_FILES["UMK"]["size"];
            
                // Verify file extension
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if(!array_key_exists($ext, $allowed)) die("Ошибка: Пожалуйста выберите принимаемый формат файлов.");
            
                // Verify file size - 20MB maximum
                $maxsize = 20 * 1024 * 1024;
                if($filesize > $maxsize) die("Ошибка: файл больше максимально допустимого размера");
            
                // Verify MYME type of the file
                if(in_array($filetype, $allowed)){
                    // Check whether file exists before uploading it
                    if(file_exists("UMKs/" . $filename)){
                        echo $filename . " уже существует.";
                    } else{
                        move_uploaded_file($_FILES["UMK"]["tmp_name"], "UMKs/" . $filename);

                    // Prepare an insert statement
                    $sql = "INSERT INTO disciplines (discipline_name , discipline_umk) VALUES (?, ?)";
                    
                    if($stmt = mysqli_prepare($link, $sql)){
                       // Bind variables to the prepared statement as parameters
                       mysqli_stmt_bind_param($stmt, "ss", $param_disciplinename, $param_umk);
                       
                       // Set parameters
                       $param_disciplinename = request("UMK_Name", 'name');
                       $param_umk = $filename;
                       
                       // Attempt to execute the prepared statement
                       if ($param_disciplinename && $param_umk)
                       {
                           if(mysqli_stmt_execute($stmt))
                               echo "Новый УМК добавлен";
                       }
                       else
                       {
                           echo "Название УМК должны быть от 3 до 15 символов и содержать только кирилиццу.";
                       }
                        }
                    } 
                } else{
                    echo "Ошибка: не приемлимый медиа тип файла."; 
                }
            } else{
                echo "Ошибка: " . $_FILES["UMK"]["error"];
            }
        }
        if($role=="operator"||$role=="admin")
        echo '
        <div class="text-center">
            <form action="'. htmlspecialchars($_SERVER["PHP_SELF"]) . '" method="post" enctype="multipart/form-data">
                <h4 class="h4 mb-3 font-weight-normal">Добавить дисциплину</h4>
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="UMK_Name" class="form-control">
                </div>    
                <div class="form-group">
                    <label>УМК</label>
                    <input type="file" name="UMK" id="fileSelect" class="form-control">
                    <p><strong>Внимание:</strong> Поддерживаются только .docx файлы менее 20 МБ</p>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Загрузить" >
                    <input type="reset" class="btn btn-default" value="Сброс">
                </div>  
            </form>
        </div>
        ';
    }
?>