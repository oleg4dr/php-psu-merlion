<?php
    function request($var, $type = 'str', $default = false){
        if (isset($_REQUEST[$var]))
        {
            switch ($type)
            {
                case 'int':
                    return is_numeric($_REQUEST[$var])
                        ? intval($_REQUEST[$var])
                        : ($default ? $default : 0);
                    break;
                case 'str':
                    if (isset($_REQUEST[$var]))
                    {
                        //приведение строки к безопасному виду
                        return htmlspecialchars($_REQUEST[$var]);
                    }
                    else
                    {
                        return $default;
                    }
                    break;
                case 'email':
                    if (preg_match("/^([a-zA-Z0-9\._-]+)@([a-zA-Z0-9\._-]+)\.([a-zA-Z]{2,4})$/ui", $_REQUEST[$var]))
                    {
                        return $_REQUEST[$var];
                    }
                    else
                    {
                        return $default;
                    }
                    break;
                case 'name':
                    if(preg_match("([а-яА-я]{3,15})", $_REQUEST[$var]))
                    {
                        return htmlspecialchars($_REQUEST[$var]);
                    }
                    else
                    {
                        return false;
                    }
                    break;
            }
        }
        else
        {
            return $default;
        }

    }

?>