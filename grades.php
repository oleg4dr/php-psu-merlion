<?php
    function getGrades(){
        require_once 'config.php';
        $disc_id = (int) $_GET['disc_id'];
        $role = $_SESSION["role"];
        $my_id=$_SESSION['id'];
        //таблица
        if($role!="student")
            $grades_q=mysqli_query($link,"SELECT grade_id, grade_kt, grade_val, userfio.userFIO_name, userfio.userFIO_surname, userfio.userFIO_middle_name, groups.group_name 
            FROM grades INNER JOIN userfio ON grades.grade_student_id=userfio.userFIO_id
            INNER JOIN groups ON groups.group_id=(SELECT students.group_id FROM students WHERE students.student_id=grade_student_id)");
        else 
            $grades_q=mysqli_query($link,"SELECT grade_id, grade_kt, grade_val, userfio.userFIO_name, userfio.userFIO_surname, userfio.userFIO_middle_name, groups.group_name 
            FROM grades INNER JOIN userfio ON grades.grade_student_id=userfio.userFIO_id
            INNER JOIN groups ON groups.group_id=(SELECT students.group_id FROM students WHERE students.student_id=grade_student_id)
            WHERE grades.grade_student_id=$my_id");
        echo '
        <h4 id="title" class="text text-center mt-3" dis_id="'.$disc_id.'">Успеваемость для дисциплины#'.$disc_id.'</h4>
        <div id="content-table">
        <table class="table mt-5 table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Группа</th>
                    <th scope="col">Студент</th>
                    <th scope="col">КТ</th>
                    <th scope="col">Оценка</th>
                </tr>
            </thead>
            <tbody id="grade_tbody">';
        while ($oneGrade =  mysqli_fetch_array($grades_q)){
            echo '
            <tr>
                <th scope="row">'. $oneGrade['grade_id'] .'</th>
                <td>'. $oneGrade['group_name'] .'</td>
                <td>'. $oneGrade['userFIO_surname'].' '.$oneGrade['userFIO_name'].' '.$oneGrade['userFIO_middle_name'] .'</td>
                <td>'. $oneGrade['grade_kt'] .'</td>
                <td>'. $oneGrade['grade_val'] .'</td>
            </tr>';
        }
        echo '
        </tbody>
        </table>
        </div>';


        $group_q=mysqli_query($link,"SELECT * FROM groups");
        //меню добавления оценок
        if($role!="student")
        {
            echo '
            <script src="../updateGrades.js"></script>
            <form id="grade_form">   
                <div class="form-row mt-3">
                    <div class="col">
                        <select class="custom-select" id="my_grouppicker">
                        <option value="" disabled selected>Группа</option>';
                            while($oneG = mysqli_fetch_array($group_q))
                                echo '<option g_id="'.$oneG['group_id'].'">'.$oneG['group_name'].'</option>';
                        echo'    
                        </select>
                    </div>
                    <div class="col">
                        <select class="custom-select" id="my_studentpicker">
                            <option value="" disabled selected>Студент</option>             
                        </select>
                    </div>
                    <div class="col">
                        <select class="custom-select" id="my_ktpicker">
                            <option value="" disabled selected>КТ</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="col">
                    <select class="custom-select" id="my_gradepicker">
                        <option value="" disabled selected>Оценка</option>
                        <option>Н</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                </div>
                <div class="form-group mt-3 text-center">
                    <input type="submit" class="btn btn-primary" id="submit_btn" value="Добавить">
                    <input type="reset" class="btn btn-default" value="Сброс">
                </div>
                <div class="container">
                    <div id="msg" class="bg-success text-white text-center rounded row justify-content-center"></div>
                </div> 
            </form>';
        }
        
    }
?>