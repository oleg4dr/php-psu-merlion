<?php
    function addPagination($tablename, $currentpage, $link, $rowsperpage){
        $query = mysqli_query($link,"SELECT COUNT(*) FROM $tablename");
        $data = mysqli_fetch_assoc($query);
        $numrows = $data['COUNT(*)'];
        // сколько страниц надо чтобы отобразить все записи
        $totalpages = ceil($numrows / $rowsperpage);
        //пагинация
        echo '
        <nav aria-label="Page navigation">
            <ul class="pagination justify-content-center">';
        //ссылки на начало
        if ($currentpage > 1){
            echo '<li class="page-item"><a class="page-link" href="'.$tablename.'?currentpage=1"><<</a></li>';
            $prevpage = $currentpage - 1;
            echo '<li class="page-item"><a class="page-link" href="'.$tablename.'?currentpage='.$prevpage.'"><</a></li>';
        }
        //сколько ссылок показывать вокруг активной страницы
        $range = 2;
        for ($x = ($currentpage - $range); $x < (($currentpage + $range)  + 1); $x++) {
            if (($x > 0) && ($x <= $totalpages)) {
                if ($x == $currentpage) {
                    echo '<li class="page-item active"><a class="page-link">'.$x.'</a></li>';
                } else {
                    echo '<li class="page-item"><a class="page-link" href="'.$tablename.'?currentpage='.$x.'">'.$x.'</a></li>';
                } 
            }
        } 
        //ссылки на конец
        if ($currentpage != $totalpages){
            $nextpage = $currentpage + 1;
            echo '<li class="page-item"><a class="page-link" href="'.$tablename.'?currentpage='.$nextpage.'">></a></li>';
            echo '<li class="page-item"><a class="page-link" href="'.$tablename.'?currentpage='.$totalpages.'">>></a></li>';
        }
        echo '</ul></nav>';
    }
?>