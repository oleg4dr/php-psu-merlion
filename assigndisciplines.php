<?php

    require_once 'config.php';
    
    $user = $_POST['user_id'];
    $disciplines = $_POST['disciplines_list'];
    //получить все id дисциплин и удалить те которых нет
    $disciplines_q = mysqli_query($link, "SELECT * FROM disciplines");
    
    while($oneDisc = mysqli_fetch_array($disciplines_q))
    if(!in_array($oneDisc['discipline_id'], $disciplines)){
        $val = $oneDisc['discipline_id'];
        mysqli_query($link, "DELETE FROM tutor_discipline WHERE tutor_discipline_tutor_id=$user AND tutor_discipline_discipline_id=$val");
    }
        

    //вставить 
    $q = "INSERT IGNORE INTO tutor_discipline (tutor_discipline_tutor_id, tutor_discipline_discipline_id) VALUES ";
    foreach($disciplines as $val){
        $q .= "($user, $val), ";
    }
    $q = substr($q, 0, -2); //убираем запятую
    $query = mysqli_query($link, $q);
    die(json_encode(array(
        'query' => $q,
    )));

?>